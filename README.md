This is a simple audio decoder plug-in for the semi popular
foobar2000 audio player for Microsoft Windows, and soon to
be other platforms as well.

I deduced the music format used by the Sega/Mega CD title,
Lunar: Eternal Blue, and unlike other software of the time,
I also deduced the interleave and header flag of the two
stereo tracks featured in the game. The sample rate was
calculated from the DAC rate used by the game in an emulator.
The header of each track also contains information on which
sector offset is the loop start point, as well as the sample
offset into the last sector for the loop end point.

The metadata was shamelessly ripped from a Lunar: Eternal
Blue music converter tool back in 2002 or 2003, a tool
which never supported the stereo tracks. Sadly, I did not
make any effort to help the author of that tool with my
information. I wasn't much of a community player at the time,
and probably didn't even think to look for contact information
with the tool. I don't even remember which tool it was, but
it was some Windows specific thing, closed source. Heh, and
this software remained closed source all this time, until now.

Enjoy. Or don't.

